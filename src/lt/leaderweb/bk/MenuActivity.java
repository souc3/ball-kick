package lt.leaderweb.bk;

import lt.leaderweb.bk.game.Level;
import lt.leaderweb.bk.game.component.scenario.Scenario;
import lt.leaderweb.bk.game.component.scenario.SurvivorScenario;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class MenuActivity extends AbstractActivity implements View.OnClickListener {
	
	public static final String VIEW_ID_EXTRA = "viewId";
	private int currentViewId = 0;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        int viewId = this.getIntent().getIntExtra(VIEW_ID_EXTRA, 0);
        if (viewId==0) {
        	viewId = R.layout.menu_main;
        }
        showView(viewId);
    }
    
    
    private void exitApp() {
    	Intent intent = new Intent(Intent.ACTION_MAIN);
    	intent.addCategory(Intent.CATEGORY_HOME);
    	intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    	startActivity(intent);
    }
    
    private void startGameAction(View v, Class<? extends Scenario> scenario, Level level) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
    	intent.setClassName(v.getContext(), GameActivity.class.getName());
    	intent.putExtra("scenario", scenario);
    	intent.putExtra("level", level);
    	intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
    	startActivity(intent);
    }
    
    private void menuActivity(View v, int viewId) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
    	intent.setClassName(v.getContext(), MenuActivity.class.getName());
    	intent.putExtra(VIEW_ID_EXTRA, viewId);
    	startActivity(intent);
    }
    
    private void showView(int viewId) {
    	setContentView(viewId);
    	switch(viewId) {
    		case R.layout.menu_main:
    	        ((Button) findViewById(R.id.start_game)).setOnClickListener(this);
    	        ((Button) findViewById(R.id.exit_app)).setOnClickListener(this);
    	        break;
    		case R.layout.menu_start:
    	        ((Button) findViewById(R.id.start_quick)).setOnClickListener(this);
    	        ((Button) findViewById(R.id.back_to_main)).setOnClickListener(this);
    	        break;
    		case R.layout.menu_start_quick:
    	        ((Button) findViewById(R.id.start_quick_easy)).setOnClickListener(this);
    	        ((Button) findViewById(R.id.start_quick_medium)).setOnClickListener(this);
    	        ((Button) findViewById(R.id.start_quick_hard)).setOnClickListener(this);
    	        ((Button) findViewById(R.id.start_quick_insane)).setOnClickListener(this);
    	        break;
    	}
    	
    }
    
    public void setContentView(int viewId) {
    	super.setContentView(viewId);
    	this.currentViewId = viewId;
    }

	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.start_game:
				menuActivity(v, R.layout.menu_start);
		    	break;
			case R.id.start_quick:
				menuActivity(v, R.layout.menu_start_quick);
				break;
			case R.id.start_quick_easy:
				startGameAction(v, SurvivorScenario.class, Level.easy);
				break;
			case R.id.start_quick_medium:
				startGameAction(v, SurvivorScenario.class, Level.medium);
				break;
			case R.id.start_quick_hard:
				startGameAction(v, SurvivorScenario.class, Level.hard);
				break;
			case R.id.start_quick_insane:
				startGameAction(v, SurvivorScenario.class, Level.insane);
				break;
			case R.id.back_to_main:
				menuActivity(v, R.layout.menu_main);
				break;
			case R.id.exit_app:
				exitApp();
				break;
		}
	}
	
	public void onBackPressed() {
		if (currentViewId != R.layout.menu_main) {
			super.onBackPressed();
		}
	}
}