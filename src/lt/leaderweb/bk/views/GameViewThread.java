package lt.leaderweb.bk.views;

import lt.leaderweb.bk.game.Constants;
import lt.leaderweb.bk.game.VisualSettings;
import lt.leaderweb.bk.game.components.Component;
import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class GameViewThread extends Thread {

	private boolean done = false;
	private SurfaceHolder surfaceHolder;
	private Component rootComponent;
	
	public GameViewThread(SurfaceHolder surfaceHolder, Component rootComponent) {
		this.surfaceHolder = surfaceHolder;
		this.rootComponent = rootComponent;
	}
	
	@Override
	public synchronized void run() {
		Canvas canvas;
		while(!done) {
			canvas = surfaceHolder.lockCanvas();
			rootComponent.draw(canvas, VisualSettings.getCurrent());
			surfaceHolder.unlockCanvasAndPost(canvas);
			try {
				wait(Constants.RedrawTimeoutInMs); //redraws will be performed on notify() calls
			} catch (InterruptedException e) {}
		}
	}
	
	public void requestFinish() {
		done = true;
		try {
			join();
		} catch(InterruptedException e) {}
	}

}
