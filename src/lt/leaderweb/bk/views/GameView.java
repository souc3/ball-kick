package lt.leaderweb.bk.views;

import lt.leaderweb.bk.game.components.GameFrame;
import android.content.Context;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {
	
	private GameFrame frame;
	private GameViewThread gameViewThread;
	
	public GameView(Context context, GameFrame frame) {
		super(context);
		this.frame = frame;
		getHolder().addCallback(this);
	}
	
	public void setFrame(GameFrame frame) {
		this.frame = frame;
	}

	public void surfaceCreated(SurfaceHolder holder) {
		gameViewThread = new GameViewThread(holder, frame);
		gameViewThread.start();
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width,	int height) {
		this.frame.updateDisplay(width, height);
	}
	
	public void destroy() {
		this.surfaceDestroyed(getHolder());
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		if (gameViewThread!=null)
			gameViewThread.requestFinish();
		gameViewThread = null;
	}
    
    @Override
    public boolean onTouchEvent(MotionEvent e) {
    	this.frame.onTouchEvent(e);
    	return true;
    }
	
	public void redraw() {
		if (gameViewThread!=null){
			synchronized(gameViewThread) {
				gameViewThread.notify();
			}
		}
	}

}
