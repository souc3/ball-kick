package lt.leaderweb.bk.game;

import java.lang.reflect.InvocationTargetException;

import lt.leaderweb.bk.game.component.scenario.Scenario;
import lt.leaderweb.bk.game.components.GameFrame;
import android.view.Display;

public class GameManager {
	
	private static GameManager instance;
	private GameRunnable game;
	
	/**
	 * Returns game manager
	 * @return
	 */
	
	public static GameManager getInstance() {
		if (instance==null) {
			instance = new GameManager();
		}
		return instance;
	}
	
	/**
	 * Returns true if game is loaded
	 * @return
	 */
	
	public synchronized boolean isGameLoaded() {
		return this.game != null;
	}
	
	/**
	 * Loads game by user parameters 
	 * @param game
	 * @param viewHandler
	 * @param display
	 */

	public synchronized void loadGame(Class<? extends Scenario> scenario, Level level, 
			GameFlowViewHandler viewHandler, Display display) {
		Scenario scenarioInstance = null;
		
		try {
			scenarioInstance = scenario.getConstructor(Level.class).newInstance(level);
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		} catch (IllegalArgumentException e) {
			throw new RuntimeException(e);
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		}
		loadGame(new GameRunnable(scenarioInstance, viewHandler), display);
	}
	
	/**
	 * Loads game runnable
	 * @param game
	 * @param display
	 */
	
	public synchronized void loadGame(GameRunnable game, Display display) {
		game.load(new GameFrame(display, game.getScenario()));
		new Thread(game).start();
		//set last
		this.game = game;
	}
	
	/**
	 * Disposes game runnable
	 */
	
	public synchronized void disposeGame() {
		this.game.end();
		this.game = null;
	}
	
	/**
	 * Returns game runnable
	 * @return
	 */
	public synchronized GameRunnable getGameRunnable() {
		return this.game;
	}
	

}
