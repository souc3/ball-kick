package lt.leaderweb.bk.game;

import android.graphics.Paint;
import android.graphics.Paint.Style;

public final class Constants {
	
	private Constants() {}
	
	public static final int LOGLEVEL = 2;
	public static final boolean VISUAL_DEBUG = LOGLEVEL < 2;
	public static final boolean DEBUG = LOGLEVEL < 1;
	
	/**
	 * Be careful!!!
	 * Editing timeout may cause concurrency issues.
	 */

	public static final int TickTimeoutInMs = 15;
	public static final int RedrawTimeoutInMs = 20;
	
	public static final float ShellSpeadInPixelsPerTick = 30;
	public static final float CollisionDistanceRatio = 0.8f;
	public static final float G = 1.2f;

	public static final Paint DebugText50While = new Paint();
	public static final Paint DebugText20While = new Paint();
	public static final Paint DebugStroke50While = new Paint();
	public static final Paint ScoreBoardBg = new Paint();
	public static final Paint ScoreBoardText = new Paint();

	public static final float halfSqrt3 = new Double(Math.sqrt(3)*0.5).floatValue();
	public static final float sqrt2 = new Double(Math.sqrt(2)).floatValue();
	
	static {
		DebugText50While.setColor(0x80ffffff);
		DebugText20While.setColor(0x33ffffff);
		DebugStroke50While.setColor(0x80ffffff);
		DebugStroke50While.setStyle(Style.STROKE);
		DebugStroke50While.setStrokeWidth((float) 1.0);
		
		ScoreBoardBg.setColor(0xff666666);
		ScoreBoardText.setColor(0xff88ffff);
	}
}
