package lt.leaderweb.bk.game;

public class Utils {
	
	private Utils() {}
	
	public static boolean isInCircle(float sircleX, float sircleY, float sircleRadius, float pointX, float pointY) {
		return !(sircleRadius < Math.sqrt(Math.pow(pointX-sircleX, 2)+Math.pow(pointY-sircleY, 2)));
	}
	
	public static double calculateDelta(float pointX1, float pointY1, float pointX2, float pointY2) {
		return Math.sqrt(Math.pow(pointX2-pointX1, 2)+Math.pow(pointY2-pointY1, 2));
	}
	
	public static double vectorAbs(float x, float y) {
		return Math.sqrt(x*x + y*y);
	}
	
	/**
	 * Direction = CCW
	 * @param pointX1
	 * @param pointY1
	 * @param pointX2
	 * @param pointY2
	 * @return
	 */
	
	public static double calculateRotationDeg(float pointX1, float pointY1, float pointX2, float pointY2) {
		float dx = pointX2 - pointX1;
		float dy = pointY2 - pointY1;
		if (dx + dy == 0) return 0;
		return -Math.toDegrees(Math.atan2(dx, dy));
	}

}
