package lt.leaderweb.bk.game;

public class VisualSettings {
	
	private static VisualSettings current;
	
	public final float lineItemResolution;
	public final float lineItemRadius;
	public final float lineItemCollisionThreshold;
	
	private VisualSettings(float lineItemResolution) {
		this.lineItemResolution = lineItemResolution;
		this.lineItemRadius = lineItemResolution*0.5f;
		this.lineItemCollisionThreshold = lineItemResolution*Constants.CollisionDistanceRatio;
	}
	

	
	public static VisualSettings override(float width, int columns) {
		return override(width / (float)(columns));

	}
	
	private static VisualSettings override(float lineItemResolution) {
		current = new VisualSettings(lineItemResolution);
		return current;
	}
	
	public static VisualSettings getCurrent() {
		return current;
	}

}
