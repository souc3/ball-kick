package lt.leaderweb.bk.game;

/**
 * Used to navigate in Honeycomb
 * 
 *     |A|B|
 *    |L|*|R|
 *     |A|B|
 * 
 * @author dalius
 */

public class Cell {
	public static final int EXTREMUM = 25;
	private static final Cell[] instances = new Cell[EXTREMUM*EXTREMUM];
	public int x, y;
	
	private Cell(int x, int y) {
		this.x=x;
		this.y=y;
	}
	
	public static Cell getCell(int x, int y) {
		int hash = hash(x, y);
		if (instances[hash]==null) {
			instances[hash] = new Cell(x, y);
		}
		return instances[hash];
	}
	
	public static int hash(int x, int y) {
		return y*EXTREMUM+x;
	}
	
	/**
	 * Dummy does not play in game and should not
	 * appear in instances array
	 * @return
	 */
	
	public static Cell getDummy() {
		return new Cell(-1, -1);
	}


	public Cell getUpperA() {
		return getCell(x-1 + (y&0x01), y+1);
	}
	
	public Cell getUpperB() {
		return getCell(x + (y&0x01), y+1);
	}
	
	public Cell getLowerA() {
		return getCell(x-1 + (y&0x01), y-1);
	}
	
	public Cell getLowerB() {
		return getCell(x + (y&0x01), y-1);
	}
	
	public Cell getLeft() {
		return getCell(x-1, y);
	}
	
	public Cell getRight() {
		return getCell(x+1, y);
	}

	public boolean isNeighbor(Cell cell) {
		return cell == getLowerA() || cell == getLowerB() || cell == getLeft() 
				|| cell == getRight() || cell == getUpperA() || cell == getUpperB();
	}
	
	@Override
	public boolean equals(Object object) {
		return this == object;
	}
	
	@Override
	public int hashCode() {
		return hash(x, y);
	}
	
	@Override
	public String toString() {
		return String.format("Cell[%d,%d]", x, y);
	}

}
