package lt.leaderweb.bk.game.components;

import lt.leaderweb.bk.game.VisualSettings;
import android.graphics.Canvas;

public interface Component {

	public void draw (Canvas canvas, VisualSettings visualSettings);

	public boolean tick();
	
	public boolean nextFrame();
}
