package lt.leaderweb.bk.game.components;


import lt.leaderweb.bk.game.VisualSettings;
import android.graphics.Canvas;


public abstract class GameItem implements Component {
	
	protected float positionX = 0f;
	protected float positionY = 0f;

	protected float vectorX = 0f;
	protected float vectorY = 0f;

	protected float rotationAngleDeg = 0f;
	
	public synchronized void setPosition(float positionX, float positionY) {
		this.positionX = positionX;
		this.positionY = positionY;
	}
	
	public void setRotationAngleDeg(float rotationAngleDeg) {
		this.rotationAngleDeg = rotationAngleDeg;
	}
	
	

	public abstract void draw(Canvas canvas, VisualSettings visualSettings);
	
	public double getDistanceFromSurface(float x, float  y) {
		return 0;
	}

	public boolean tick() {
		throw new RuntimeException("Not implemented");
	}

	public boolean nextFrame() {
		return false;
	}
	
	

}