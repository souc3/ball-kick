package lt.leaderweb.bk.game.components;

import lt.leaderweb.bk.game.Constants;
import lt.leaderweb.bk.game.Utils;
import lt.leaderweb.bk.game.VisualSettings;

/**
 * An item that belongs to Line 
 * @author Dalius
 *
 */

public abstract class LineItem extends GameItem {
	
	public enum Color {
		RED(0x80ff0000), BLUE(0x800000ff), GREEN(0x80008800), YELLOW(0x80ff8800);
		
		private int color;
		Color(int color) {
			this.color = color;
		}
		
		public int getColor () {
			return color;
		}
	};
	
	private static int lineItemTotal = 0;
	protected final int lineItemNr;

	protected int color;
	private boolean falling;
	private int frame;

	public LineItem() {
		this.color = Color.RED.getColor();
		this.lineItemNr = ++lineItemTotal;
	}

	public void setColor(Color color) {
		this.color = color.getColor();
	}
	
	public int getColor() {
		return color;
	}

	public boolean isSameColor(LineItem lineItem) {
		return lineItem!=null && lineItem.getColor() == color;
	}
	
	public abstract int getScore();
	
	public double getDistanceFromSurface(float pointX1, float pointY1, float pointX2, float pointY2) {
		return Utils.calculateDelta(pointX1, pointY1, pointX2, pointY2) - VisualSettings.getCurrent().lineItemRadius;
	}
	
	public int getLineItemNr() {
		return lineItemNr;
	}
	
	public void fall(float x, float y) {
		frame = 0;
		falling = true;
		positionX = x;
		positionY = y;
	}
	
	public abstract void explode(float x, float y);
	
	
	public boolean nextFrame() {
		if (falling) {
			positionY -= (frame++ * Constants.G);
			if (frame>17) {
				falling = false;
				explode(positionX, positionY);
			}
		}
		return falling;
	}
	
	/**
	 * Every object is unique
	 */
	@Override
	public final boolean equals(Object object) {
		if (!(object instanceof LineItem))
			return false;
		
		if (lineItemNr != ((LineItem)object).getLineItemNr())
			return false;
		
		return true;
	}
	
	/**
	 * Should always return unique hash slot
	 */
	@Override
	public final int hashCode() {
		return lineItemNr;
	}
	
	@Override
	public String toString() {
		return String.format("{lineItemNr:%d, %d}", lineItemNr, color);
	}

	
}
