package lt.leaderweb.bk.game.components;


import static lt.leaderweb.bk.game.Constants.*;

import lt.leaderweb.bk.game.Constants;
import lt.leaderweb.bk.game.VisualSettings;
import lt.leaderweb.bk.game.component.scenario.Scenario;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.RectF;

public class Magazine extends GameItem {

	private static final int SIZE = 5;
	private static final int POSITION_FRAMES = 4;
	
	private Scenario scenario;
	private int head = 0;
	private LineItem[] queue;
	
	protected RectF boundaries;
	
	private final PointF[] positions = new PointF[(SIZE*POSITION_FRAMES) + 1];
	private int positionOffset = 0;
	
	public Magazine(Scenario scenario) {
		this.scenario = scenario;
		queue = new LineItem[SIZE];
		for (int i=0; i<SIZE; i++) {
			queue[i] = scenario.createMagazineLineItem();
		}
		for (int i=0; i < positions.length; i++) {
			positions[i] = new PointF(0, 0);
		}
	}
	

	public void updateDisplay(RectF boundaries) {
		this.boundaries = boundaries;
		this.positionX = boundaries.left +  (boundaries.width()*0.75f);
		this.positionY = boundaries.top + (boundaries.height()*0.6f);
		
		float radius = VisualSettings.getCurrent().lineItemRadius;
		
		for (int i=0; i < positions.length; i++) {
			updatePosition(i, radius);
		}
	}
	
	private void updatePosition(int index, float radius) {
		if (index < POSITION_FRAMES) {
			double stepRadius = (90*index)/POSITION_FRAMES + 180;
			double p = Math.cos(Math.toRadians(stepRadius));
			positions[index].x = sqrt2*radius * (float)p;
			p = Math.sin(Math.toRadians(stepRadius));
			positions[index].y = sqrt2*radius * (float)p;
		} else {
			positions[index].x = 2*radius*(index-POSITION_FRAMES)/POSITION_FRAMES;
			positions[index].y = -sqrt2*radius;
		}
	}
	
	public synchronized LineItem poll(boolean animate) {
		LineItem item = queue[this.head];
		queue[head] = scenario.createMagazineLineItem();
		this.head = (this.head+1)%SIZE;
		if (animate) {
			positionOffset = POSITION_FRAMES;
		}
		return item;
	}

	@Override
	public synchronized void draw(Canvas canvas, VisualSettings visualSettings) {
		int it = head;
		canvas.translate(positionX, positionY);
		canvas.scale(1, -1);

		if (Constants.VISUAL_DEBUG) {
			for (int i=0; i < positions.length; i++) {
				PointF position = positions[i];
				canvas.drawCircle(position.x, position.y, 5, Constants.DebugText20While);
			}
		}
		
		for(int i=0; i<SIZE;i++) {
			PointF position = positions[i*POSITION_FRAMES+positionOffset];
			canvas.translate(position.x, position.y);
			queue[it].draw(canvas, visualSettings);
			canvas.translate(-position.x, -position.y);
			it = (it+1)%SIZE;
		}

		canvas.scale(1, -1);
		canvas.translate(-positionX, -positionY);
	}
	
	public boolean tick() {
		if (positionOffset > 0)
			positionOffset--;
		return true;
	}

}
