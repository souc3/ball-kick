package lt.leaderweb.bk.game.components;

import lt.leaderweb.bk.game.Utils;
import lt.leaderweb.bk.game.Constants;
import lt.leaderweb.bk.game.VisualSettings;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;

public class Shot extends GameItem {
	private final float shotSpeedPxPerTick;
	private float radius;
	private LineItem item;
	private boolean fired;
	private Paint stroke;
	private boolean focused;

	public Shot(float shotSpeedPxPerTick) {
		this.shotSpeedPxPerTick = shotSpeedPxPerTick;
		this.fired = false;
		this.stroke = new Paint();
		this.stroke.setColor(0x80ffffcc);
		this.stroke.setStyle(Style.STROKE);
		this.stroke.setStrokeWidth((float) 1.0);
		this.focused = false;
	}
	
	public void updateGraphics() {
		this.radius = VisualSettings.getCurrent().lineItemRadius * 1.4f;
	}

	@Override
	public void draw(Canvas canvas, VisualSettings visualSettings) {
		Matrix original = canvas.getMatrix();
		canvas.translate(positionX, positionY);
		if (Constants.VISUAL_DEBUG) {
			canvas.drawText(String.format("x=%.2f", positionX), 50, -30, Constants.DebugText50While);
			canvas.drawText(String.format("y=%.2f", positionY), 50, -20, Constants.DebugText50While);
		}
		canvas.rotate(rotationAngleDeg);
		if (Constants.VISUAL_DEBUG) {
			canvas.drawCircle(0, 0, radius, stroke);
			canvas.drawRect(-radius*0.8f, -radius, radius*0.8f, radius, stroke);
		}
		if (this.item!=null)
			this.item.draw(canvas, visualSettings);
		canvas.setMatrix(original);
	}
	
	/**
	 * Returns true if point belongs to this area
	 * @param x
	 * @param y
	 * @return
	 */
	
	public boolean isInnerPoint(float x, float y) {
		return Utils.isInCircle(positionX, positionY, radius, x, y);
	}
	
	public void focus() {
		this.stroke.setStrokeWidth((float) 5.0);
		this.focused = true;
	}
	
	public void blur() {
		this.stroke.setStrokeWidth((float) 1.0);
		this.focused = false;
	}
	
	/**
	 * The vector from current position
	 * @param vectorX
	 * @param vectorY
	 */
	
	public synchronized void fire(float vectorX, float vectorY) {
		this.vectorX = vectorX;
		this.vectorY = vectorY;
		this.fired = true;
	}
	

	public float getVectorX() {
		return vectorX;
	}

	public float getVectorY() {
		return vectorY;
	}
	
	public void hit() {
		this.item = null;
		this.fired = false;
	}
	

	public synchronized void load() {
		this.fired = false;
	}
	
	public synchronized void load(LineItem item) {
		this.item = item;
		this.fired = false;
	}
	
	public LineItem getItem() {
		return this.item;
	}
	
	public void horizontalBounce() {
		vectorX = vectorX*-1;
		tick();
	}

	@Override
	public synchronized boolean tick() {
		if (!fired)
			return true;
		
		this.positionX += shotSpeedPxPerTick*vectorX;
		this.positionY += shotSpeedPxPerTick*vectorY;
		
		return true;
	}

	public boolean isFocused() {
		return focused;
	}

	public boolean isFired() {
		return fired;
	}

	@Override
	public double getDistanceFromSurface(float x, float y) {
		if (item!=null) {
			return item.getDistanceFromSurface(this.positionX, this.positionY, x, y);
		} else {
			return Utils.calculateDelta(this.positionX, this.positionY, x, y);
		}
	}

}
