package lt.leaderweb.bk.game.components;

import lt.leaderweb.bk.game.Constants;
import lt.leaderweb.bk.game.VisualSettings;
import android.graphics.Canvas;
import android.graphics.RectF;

public class ScoreBoard extends GameItem {
	
	private int score = 0;
	private RectF board;
	private float scoreX;
	private float scoreY;

	public void updateDisplay(RectF boundaries) {
		this.positionX = boundaries.left;
		this.positionY = boundaries.top;
		
		float height = boundaries.height();
		float width = boundaries.width();
		this.board = new RectF(height*0.1f, height*0.7f, width*0.3f, height*0.9f);
		scoreX = board.left+10;
		scoreY = board.top+20;
	}

	@Override
	public void draw(Canvas canvas, VisualSettings visualSettings) {
		canvas.translate(positionX, positionY);
		canvas.drawRect(board, Constants.ScoreBoardBg);
		canvas.drawText(String.format("%07d", score), scoreX, scoreY, Constants.ScoreBoardText);
		
		canvas.translate(-positionX, -positionY);
	}

	public void addScore(int score) {
		this.score += score;
	}

}
