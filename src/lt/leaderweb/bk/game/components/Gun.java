package lt.leaderweb.bk.game.components;

import lt.leaderweb.bk.game.Utils;
import lt.leaderweb.bk.game.Constants;
import lt.leaderweb.bk.game.VisualSettings;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.MotionEvent;

public class Gun extends GameItem {

	private final GameRoot hitAcceptor;
	private final Shot shot;
	//gun width and height
	private float width, height;
	private float rotationAngleDeg;
	private final float gunVieportDeg = 70f; 
	private final double halfPI = Math.PI/2;
	
	private final Magazine magazine;

	private Paint paint;
	
	public Gun(GameRoot hitAcceptor, Magazine magazine) {
		this.hitAcceptor = hitAcceptor;
		this.magazine = magazine;
		this.paint = new Paint();
		this.paint.setColor(0x80ffffff);
		this.shot = new Shot(Constants.ShellSpeadInPixelsPerTick);
		this.shot.load(magazine.poll(false));
	}
	

	public void updateDisplay(RectF boundaries) {
		
		this.positionX = boundaries.width()*0.5f;
		this.positionY = boundaries.top + boundaries.height()*0.2f;
		
		this.height = boundaries.height() * 0.6f;
		this.width = height*0.2f;
		
		this.shot.setPosition(positionX, positionY);
		this.shot.updateGraphics();
		this.magazine.updateDisplay(boundaries);
	}
	


	@Override
	public void draw(Canvas canvas, VisualSettings visualSettings) {
		this.shot.draw(canvas, visualSettings);
		this.magazine.draw(canvas, visualSettings);
		Matrix original = canvas.getMatrix();
		canvas.translate(positionX, positionY);
		if (Constants.VISUAL_DEBUG) {
			canvas.drawText(String.format("aim x=%.3f", getAimXVector()), -30, -30, paint);
			canvas.drawText(String.format("aim y=%.3f", getAimYVector()), -30, -20, paint);
			canvas.drawText(String.format("r=%.2f deg", rotationAngleDeg), -30, -10, paint);
		}
		
		canvas.rotate(rotationAngleDeg);
		canvas.drawRect(-width*0.2f, 0, width*0.2f, height, paint);
		canvas.setMatrix(original);
	}
	
	/**
	 * Reloads gun with next shell
	 * @param newShell
	 */
	public void reload(boolean newShell) {
		this.shot.setPosition(positionX, positionY);
		if (newShell) {
			this.shot.load(magazine.poll(true));
		} else {
			this.shot.load();
		}
	}
	
	/**
	 * Sets vectors to shell and fires
	 */
	
	public void fire() {
		this.shot.fire((float)getAimXVector()*-1, (float)getAimYVector()*-1);
	}
	
	/**
	 * The aim x vector 
	 * @return double [-1..1]
	 */
	
	private double getAimXVector() {
		return Math.cos(Math.toRadians(rotationAngleDeg) + halfPI);
	}
	
	/**
	 * The aim y vector
	 * @return double [-1..1]
	 */
	private double getAimYVector() {
		return Math.sin(Math.toRadians(rotationAngleDeg) + halfPI);
	}
	
	/**
	 * Aim handler
	 * @param e MotionEvent
	 */
	
	public void onTouchEvent(MotionEvent e) {
		float x = e.getX();
		float y = e.getY();
		float rotationAngleDeg = 0f;
		
		if (this.shot.isFired())
			return; //aiming disabled if no shot loaded
		
		switch (e.getAction()) {
			case MotionEvent.ACTION_DOWN:
				if (this.shot.isInnerPoint(x, y)) {
					this.shot.focus();
				}
				break;
			case MotionEvent.ACTION_MOVE:
				if (this.shot.isFocused()) {
					rotationAngleDeg = (float)Utils.calculateRotationDeg(positionX, positionY, x, y);
					
					if (Math.abs(rotationAngleDeg) > gunVieportDeg) {
						return; // exit processing out of viewport movements
					}
					this.rotationAngleDeg = rotationAngleDeg;
					
					if (!Utils.isInCircle(positionX, positionY, height, x, y)) {
						x = (float)getAimXVector()*height + positionX;
						y = (float)getAimYVector()*height + positionY;
					}
					this.shot.setRotationAngleDeg(rotationAngleDeg);
					this.shot.setPosition(x, y);
				}
				break;
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_CANCEL:
				this.shot.blur();
				if (Utils.isInCircle(positionX, positionY, height*0.7f, this.shot.positionX, this.shot.positionY)) {
					this.reload(false); // do not fire
				} else {
					this.fire();
				}
				break;
			default:
				break;
		}
		
	}

	@Override
	public boolean tick() {
		this.shot.tick();
		this.magazine.tick();
		
		if (shot.isFired()) {
			if (hitAcceptor.hitAccept(shot)) {
				this.reload(true);
			}
		}
		
		
		return true;
	}

}
