package lt.leaderweb.bk.game.components;

public interface GameRoot {
	
	public boolean hitAccept(Shot shot);
	
	public void registerAnimation(Component anomation);

}
