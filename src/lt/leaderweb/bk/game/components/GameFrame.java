package lt.leaderweb.bk.game.components;

import java.util.HashSet;
import java.util.Set;

import lt.leaderweb.bk.game.Constants;
import lt.leaderweb.bk.game.VisualSettings;
import lt.leaderweb.bk.game.component.scenario.Scenario;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.Display;
import android.view.MotionEvent;

/**
 * Responsible for balls frame
 * @author Dalius
 *
 */

public final class GameFrame extends GameItem implements GameRoot {

	public static final int OFFSET_BOTTOM = 25;//%

	//private final Scenario scenario;
	private final ActionField actionField;
	private final Gun gun;
	private final ScoreBoard scoreBoard;
	
	private final Rect backgroundRect = new Rect(0,0,0,0);
	private final Paint backgroundPaint = new Paint();
	
	private Matrix matrix;
	private Matrix matrixInv;
	private float touchX, touchY;
	

	private final Set<Component> animations = new HashSet<Component>();
	private final Set<Component> toRemove = new HashSet<Component>();
	
	public static final Paint redPaint;
	static {
		redPaint = new Paint();
		redPaint.setColor(0x80ff0000);
	}
	

	public GameFrame(Display display, Scenario scenario) {
		this(display.getWidth(), display.getHeight(), scenario);
	}
	
	public GameFrame(int width, int height, Scenario scenario) {
		this.backgroundPaint.setColor(0xff000000);
		
		//this.scenario = scenario;
		this.actionField = scenario.getActionField();
		
		this.gun = new Gun(this, scenario.getMagazine());
		this.scoreBoard = new ScoreBoard();
		
		updateDisplay(width, height);
	}
	
	public synchronized void updateDisplay(final int viewWidth, final int viewHeight) {
		
		//Optimize graphics
		final float lineItemResolution = VisualSettings.override(viewWidth, actionField.getColumns()).lineItemResolution;
		
		this.backgroundRect.set(0, 0, viewWidth, viewHeight);

		float translateTop = viewHeight - (viewHeight * (OFFSET_BOTTOM) / 100);
		float deadLine = this.actionField.getRows()*0.75f*lineItemResolution;
		
		//see less that 3/4 balls if does not fit in height 
		translateTop = Math.min(deadLine, translateTop) - lineItemResolution*0.5f;

		this.matrix = new Matrix();
		this.matrix.setValues(new float[]{
				1f,   0f,   0, 
				0f,   -1,   translateTop , 
				0f,   0f,   1f            
			});
		

		this.matrixInv = new Matrix();
		this.matrix.invert(this.matrixInv);
		
		RectF bottomPanel = new RectF(0, translateTop, viewWidth, viewHeight);
		this.gun.updateDisplay(bottomPanel);
		this.scoreBoard.updateDisplay(bottomPanel);

		//stop line
		float[] processing = new float[]{0, -lineItemResolution};

		this.matrixInv.mapPoints(processing);
		this.actionField.updateDisplay(processing[1]);
	}
	

	@Override
	public synchronized void draw(Canvas canvas, VisualSettings visualSettings) {
		canvas.drawRect(backgroundRect, backgroundPaint);
		canvas.drawCircle(touchX, touchY, 30, redPaint);

		gun.draw(canvas, visualSettings);
		scoreBoard.draw(canvas, visualSettings);

		
		canvas.setMatrix(matrix);
		if (Constants.VISUAL_DEBUG) {
			canvas.drawRect(0, 0, 480, 800, Constants.DebugText20While);
		}
		actionField.draw(canvas, visualSettings);

		for (Component animation : animations) {
			animation.draw(canvas, visualSettings);
		}
	}
	
	public void onTouchEvent(MotionEvent e) {
		this.gun.onTouchEvent(e);
		touchX = e.getX();
		touchY = e.getY();
	}
	

	@Override
	public synchronized boolean tick() {		
		return this.actionField.tick() && this.gun.tick();
	}
	
	@Override
	public synchronized boolean nextFrame() {
		for (Component animation : animations) {
			if (!animation.nextFrame()) {
				toRemove.add(animation);
			}
		}
		for (Component animation : toRemove) {
			animations.remove(animation);
		}
		toRemove.clear();
		return true;
	}
	
	public synchronized void registerAnimation(Component anomation) {
		animations.add(anomation);
	}

	public synchronized boolean hitAccept(Shot shot) {
		float[] shotPosition = new float[]{shot.positionX, shot.positionY};
		this.matrixInv.mapPoints(shotPosition);
		return this.actionField.delegateHit(shotPosition[0], shotPosition[1], shot, scoreBoard);
	}

}
