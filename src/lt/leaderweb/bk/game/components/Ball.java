package lt.leaderweb.bk.game.components;

import static lt.leaderweb.bk.game.Constants.*;

import org.apache.http.MethodNotSupportedException;

import lt.leaderweb.bk.game.VisualSettings;
import android.graphics.Canvas;
import android.graphics.Paint;

public class Ball extends LineItem {
	
	private Paint paint = new Paint();
	
	private boolean exploding;
	private int frame;
	private float radiusDisplayRatio = 1f;
	
	
	public void draw(Canvas canvas, VisualSettings visualSettings) {
		paint.setColor(color);
		canvas.translate(positionX, positionY);
		canvas.drawCircle(0, 0, visualSettings.lineItemRadius * radiusDisplayRatio, paint);
		if (VISUAL_DEBUG) {
			canvas.drawCircle(0, 0, 2, DebugText50While);
			
			Paint paint = new Paint();
			paint.setColor(0xffffffff); 
			paint.setTextSize(20); 
			canvas.drawText(String.format("%d", lineItemNr), 10, 25, paint);
		}

		canvas.translate(-positionX, -positionY);
	}

	public boolean tick() {
		throw new RuntimeException(new MethodNotSupportedException("Not implemented"));
	}

	@Override
	public int getScore() {
		return 5;
	}

	@Override
	public void explode(float x, float y) {
		frame = 0;
		radiusDisplayRatio = 1f;
		exploding = true;
		positionX = x;
		positionY = y;
	}
	
	public boolean nextFrame() {
		if (exploding) {
			radiusDisplayRatio *= 0.9f;
			
			if(frame++>10) {
				exploding = false;
			}
			
		}
		
		return super.nextFrame() || exploding;
	}

}
