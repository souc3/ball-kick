package lt.leaderweb.bk.game.components;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import lt.leaderweb.bk.game.Cell;
import lt.leaderweb.bk.game.Constants;
import lt.leaderweb.bk.game.GameManager;
import lt.leaderweb.bk.game.Utils;
import lt.leaderweb.bk.game.VisualSettings;
import static lt.leaderweb.bk.game.Constants.*;
import android.graphics.Canvas;
import android.graphics.Point;
import android.util.Log;

/**
 * This is default ActionField implementation. The grid game line is
 * constructed of 2d array holding the line items (balls). Every second row is shifted
 * by the half of ball size to imitate honeycomb.
 * 
 * The last two lines (block) are removed from the bottom of the line and appended to the end
 * of array with the new balls once it reaches deadline.
 * 
 * @author dalius
 *
 */

public abstract class GridGameLine extends GameItem implements ActionField {
	
	private static final String HIT_TAG = "Hit";
		
	//Meta info
	protected final int rows, columns;

	//State objects
	protected final LineItem[][] grid;
	private final LineItem[][] handler;
	
	/* 
	 * The positioning deltas. 
	 * Used for counting line item positions
	 */
	protected float deltaShiftX;
	protected float deltaShiftY;
	protected float halfDeltaShiftY;
		
	//Y position Block Refill threshold
	protected float refillThreshold;
	
	//hit processing top boundary
	protected float stopLine;
	protected float bounceLeft;
	protected float bounceRight;

	private Cell shotCell = Cell.getDummy();
	private Cell shotPreviousCell = Cell.getDummy();
	//the lowest index of line containing at least one line item
	protected int bottomLineIndex;
	
	public List<Point> trace = new ArrayList<Point>();
	
	private GameRoot root;
	
	/**
	 * Initializes grid of game line. The initial parameter of columns is
	 * used to calculate the blocks (each containing two rows).
	 * @param columns
	 */
	
	public GridGameLine(int columns) {
		
		int blocks = 1*columns; //the size of table 
		this.rows = blocks*2 + 1;
		this.columns = columns;
		
		if (this.rows>Cell.EXTREMUM || this.columns>Cell.EXTREMUM) {
			throw new IllegalStateException("Grid extremums are over the Cell extremums");
		}
		
		this.bottomLineIndex = rows; // out of grid
		this.grid = new LineItem[rows][columns+2];
		this.handler = new LineItem[2][];
	}
	
	public void updateDisplay(float stopLine) {
		this.stopLine = stopLine;
		
		final float lineItemResolution = VisualSettings.getCurrent().lineItemResolution;
		this.deltaShiftX = 0.5f*lineItemResolution;
		this.deltaShiftY = halfSqrt3*lineItemResolution;
		this.halfDeltaShiftY = deltaShiftY*0.5f;
		
		this.refillThreshold = 2f * deltaShiftY;
		
		
		float shiftRight = (0.25f*lineItemResolution);
		
		this.positionX = (-1f*lineItemResolution) + shiftRight;
		this.bounceLeft = shiftRight;
		this.bounceRight = shiftRight + lineItemResolution*columns - deltaShiftX;
	}

	protected abstract void createLineItem(int x, int y);

	protected final void addLineItem(LineItem item, int x, int y) {
		grid[y][x] = item;
		bottomLineIndex = Math.min(bottomLineIndex, y);
	}

	protected void removeLineItem(int x, int y) {
		grid[y][x] = null;
	}
	
	/**
	 * Removes line item from cell an collects falling items
	 * @param cell
	 * @param fallBag
	 */
	
	protected void removeLineItem(Cell cell, Set<Cell> fallBag) {
		if (grid[cell.y][cell.x]!=null) {
			grid[cell.y][cell.x].explode(getActualX(cell), getActualY(cell));
			getRoot().registerAnimation(grid[cell.y][cell.x]);
		}
		removeLineItem(cell.x, cell.y);
		collectFaling(cell.getLowerA(), fallBag);
		collectFaling(cell.getLowerB(), fallBag);
	}
	
	/**
	 * Collects falling balls from line
	 * @param cell
	 * @param fallBag
	 */
	
	private void collectFaling(Cell cell, Set<Cell> fallBag) {
		if(cell.y < 0 || grid[cell.y][cell.x]==null)
			return;
		Cell a = cell.getUpperA();
		Cell b = cell.getUpperB();
		if ((grid[a.y][a.x]==null || fallBag.contains(a)) && (grid[b.y][b.x]==null || fallBag.contains(b))) {
			fallBag.add(cell);
			if (cell.y > 0) {
				collectFaling(cell.getLowerA(), fallBag);
				collectFaling(cell.getLowerB(), fallBag);
			}
		}
	}
	
	
	private Cell resolveCell(float shotX, float shotY) {
		
		//find y index in the grid that is nearest to shot center
		final int shotYIndex = (int)Math.floor((shotY + halfDeltaShiftY - positionY) / deltaShiftY);
		//shift shot x for odd rows
		final float shiftedShotX = shotX + (shotYIndex & 0x01)*-deltaShiftX;
		//find x index in the grid that is nearest to shot center
		final int shotXIndex = (int)Math.ceil((shiftedShotX-deltaShiftX) / VisualSettings.getCurrent().lineItemResolution) + 1;
		

		//validate cell
		if (!isValid(shotXIndex, shotYIndex)) {
			if (DEBUG) Log.d(HIT_TAG, String.format("Not valid [%d,%d]", shotXIndex, shotYIndex));
			return null;
		}
		
		return Cell.getCell(shotXIndex, shotYIndex);
	}
	
	
	public synchronized boolean delegateHit(final float shotX, final float shotY, Shot shot, ScoreBoard scoreBoard) {
		
		if (DEBUG) Log.d(HIT_TAG, String.format("Try on shotX=%2$f, shotY=%2$f", shotX, shotY));
		//trace.add(new Point((int)shotX, (int)shotY));
		
		//void shot if it is out of the boundaries
		if (shotY > stopLine) {
			return true;
		}
		
		//bounce
		if (shotX > bounceRight || shotX < bounceLeft) {
			shot.horizontalBounce();
		}
		Cell cell = resolveCell(shotX, shotY);
		
		if (cell==null) {
			return false;
		}

		shotPreviousCell = shotCell;
		shotCell = cell;
		
		if (DEBUG) Log.d(HIT_TAG, String.format("Shot cell is %s", shotCell));
		
		
		//Process to previous cell if we hit existing one
		if (grid[shotCell.y][shotCell.x] != null) {
			if (shotCell.isNeighbor(shotPreviousCell)) {
				Cell a = shotPreviousCell.getUpperA();
				Cell b = shotPreviousCell.getUpperB();
				if (grid[a.y][a.x] != null || grid[b.y][b.x] != null) {
					precessHit(shot.getItem(), shotPreviousCell, scoreBoard);
				}else {
					shot.horizontalBounce();
					return false;
				}
			} else {
				//Explode the ball it cannot be placed in previous cell
				GameRoot root = GameManager.getInstance().getGameRunnable().getFrame();
				shot.getItem().explode(shotX, shotY);
				root.registerAnimation(shot.getItem());
			}
			return true;
		}

		Cell a = shotCell.getUpperA();
		Cell b = shotCell.getUpperB();
		
		boolean hit = checkTouch(shotX, shotY, shot, a, !isValid(b.x, b.y))
				|| checkTouch(shotX, shotY, shot, b, !isValid(a.x, a.y));
		
		if (hit) {
			precessHit(shot.getItem(), shotCell, scoreBoard);
			return true;
		}		
		
		return false;
	}
	
	/**
	 * Checks if shot touches the ball in given cell
	 * @param shotX
	 * @param shotY
	 * @param shot
	 * @param targetXIndex
	 * @param targetYIndex
	 * @return
	 */
	
	private boolean checkTouch(float shotX, float shotY, Shot shot, Cell targetCell, boolean ignoreDistance) {
		if (DEBUG) Log.d(HIT_TAG, String.format("Target %s", targetCell));
		if (grid[targetCell.y][targetCell.x]==null) return false;

		
		return ignoreDistance || Utils.calculateDelta(shotX, shotY, getActualX(targetCell), getActualY(targetCell)) 
				<= VisualSettings.getCurrent().lineItemCollisionThreshold;
	}
	
	/**
	 * Validates indexes against grid
	 * @param x
	 * @param y
	 * @return
	 */
	
	private boolean isValid(int x, int y) {
		return !(x < 1 || x > columns || y < 0 || y > rows);
	}
	
	/**
	 * Adds line item to line and processes post hit actions
	 * @param item
	 * @param shotYIndex
	 * @param shotXIndex
	 * @param odd
	 */
	
	protected abstract void precessHit(LineItem item, Cell shotCell, ScoreBoard scoreBoard);
	
	/**
	 * A center X coordinate of cell
	 * @param cell
	 * @return
	 */
	
	public float getActualX(Cell cell) {
		return cell.x * VisualSettings.getCurrent().lineItemResolution + positionX + (cell.y & 0x01)*deltaShiftX;
	}
	
	/**
	 * A center Y coordinate of cell
	 * @param cell
	 * @return
	 */
	
	public float getActualY(Cell cell) {
		return cell.y * deltaShiftY + positionY;
	}
	
		
	public int getRows() {
		return rows;
	}

	public int getColumns() {
		return columns;
	}
		
	public synchronized void appendBlock() {

		this.bottomLineIndex -= 2;
		//save closest lines
		handler[0] = this.grid[0];
		handler[1] = this.grid[1];
		//shift rest
		for (int y=0;y<rows-3;y++) {
			this.grid[y] = this.grid[y+2];
		}
		//move saved to the end of map
		this.grid[rows-3] = handler[0];
		this.grid[rows-2] = handler[1];
		
		//refill end of map
		for (int y=rows-3; y < rows-1; y++) {
			for (int x=1; x < columns+1; x++) {
				//remove if exists
				if (this.grid[y][x]!=null) 
					this.removeLineItem(x, y);
				//create item
				this.createLineItem(x, y);
			}
		}
	}

	public synchronized void prependBlock() {
		
	}
	
	@Override
	public void draw(Canvas canvas, VisualSettings visualSettings) {
		
		if (VISUAL_DEBUG) {
			for (Point point: trace) {
				canvas.drawCircle(point.x, point.y, 2, Constants.DebugText50While);
			}
			canvas.drawText(String.format("%d", bottomLineIndex), 50, -30, Constants.DebugText50While);
		}
		
		canvas.save();
		canvas.translate(positionX, positionY);

		float translateX = 0, translateY = 0;
		for (int y=0; y < rows-1; y++) {
			for (int x=1; x < columns+1; x++) {
				if (grid[y][x]!=null) {
					translateX = x * visualSettings.lineItemResolution + (y & 0x01) * deltaShiftX;
					translateY = y * deltaShiftY;
					canvas.translate(translateX, translateY);					
					grid[y][x].draw(canvas, visualSettings);
					canvas.translate(-translateX, -translateY);
				}
				
				if (VISUAL_DEBUG) {
					translateX = x * visualSettings.lineItemResolution;
					translateY = y * deltaShiftY;
					if (y%2==1) {
						translateX += deltaShiftX;
					}
					
					canvas.translate(translateX, translateY);	
					canvas.drawRect(visualSettings.lineItemResolution*-0.5f+1, 
							deltaShiftY*-0.5f+1, 
							visualSettings.lineItemResolution*0.5f-1, 
							deltaShiftY*0.5f-1, 
							Constants.DebugStroke50While);
					canvas.translate(-translateX, -translateY);
					
					if (shotCell.y == y && shotCell.x == x) {
						canvas.drawCircle(translateX, translateY, 20, Constants.DebugText50While);
					} else {
						canvas.drawCircle(translateX, translateY, 20, Constants.DebugText20While);
					}
				}

			}
		}

		canvas.restore();
		
	}

	@Override
	public synchronized boolean tick() {
		//positionY varies from 0 to -refillThreshold
		this.positionY += vectorY;
		
		if (isGameOver()) return false;
		
		if (this.positionY > -this.refillThreshold)
			return true;
		
		//Threshold reached:
		
		this.appendBlock();
		this.positionY += this.refillThreshold;
		return true;
	}
	
	protected boolean isGameOver() {
		if (bottomLineIndex<(-this.positionY/deltaShiftY))
			return true;// game over
		return false;
	}
	
	private GameRoot getRoot() {
		if (root==null) {
			root = GameManager.getInstance().getGameRunnable().getFrame();
		}
		return root;
	}
}
