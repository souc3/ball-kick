package lt.leaderweb.bk.game.components;

/**
 * A field where the main game action appears.
 * The field contains balls an handles shots from
 * the gun
 * 
 * 
 * @author dalius
 *
 */

public interface ActionField extends Component {
	
	public void appendBlock();
	
	public void updateDisplay(float stopLine);
	
	public int getRows();

	public int getColumns();
	
	public boolean delegateHit(final float shotX, final float shotY, Shot shot, ScoreBoard scoreBoard);

}
