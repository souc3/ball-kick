package lt.leaderweb.bk.game;

public interface GameFlowViewHandler {
	
	public void showGameOverView();
	
	public void showErrorView(RuntimeException ex);
	
	public void redraw();

}
