package lt.leaderweb.bk.game;

import android.util.Log;
import lt.leaderweb.bk.game.component.scenario.Scenario;
import lt.leaderweb.bk.game.components.GameFrame;


public class GameRunnable implements Runnable {
	
	public enum Status {
		WAITING, ON, PAUSED, OVER
	}

	
	private Status status;
	private GameFrame frame;
	private GameFlowViewHandler viewHandler;
	private Scenario scenario;
	
	public GameRunnable(Scenario scenario, GameFlowViewHandler viewHandler) {
		status = Status.WAITING;
		this.scenario = scenario;
		this.viewHandler = viewHandler;
	}
	
	public synchronized void load(GameFrame frame) {
		if (frame == null) throw new IllegalArgumentException("No game frame?");
		this.frame = frame;
	}
	
	public synchronized void start() {
		if (status == Status.WAITING) {
			status = Status.ON;
		}
	}
	
	public synchronized void pause() {
		if (status == Status.ON) {
			status = Status.PAUSED;
		}
	}
	
	public synchronized void resume() {
		if (status == Status.PAUSED) {
			status = Status.ON;
		}
	}
	
	public synchronized void end() {
		status = Status.OVER;
	}
	
	public Status getStatus() {
		return status;
	}

	public void run() {
		long start = 0;
		long sleep = 0;
		try {
			while (status != Status.OVER) {
				
				start = System.nanoTime();
				synchronized(this) {
					if (status == Status.ON) {
						if(!frame.tick()) {
							end();//game over
							viewHandler.showGameOverView();
						}
						frame.nextFrame();
					}
					viewHandler.redraw();
				}
	
				try {
					sleep = Constants.TickTimeoutInMs - ((System.nanoTime()-start)/1000000);
					Thread.sleep(Math.max(Constants.TickTimeoutInMs, sleep));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} catch (RuntimeException e) {
			viewHandler.redraw();
			Log.e("GameRunnable", "Game has crashed", e);
			viewHandler.showErrorView(e);
		}
	}

	public GameFrame getFrame() {
		return frame;
	}

	public Scenario getScenario() {
		return scenario;
	}
	
}
