package lt.leaderweb.bk.game.component.scenario;

import lt.leaderweb.bk.game.components.ActionField;
import lt.leaderweb.bk.game.components.LineItem;
import lt.leaderweb.bk.game.components.Magazine;

/**
 * Scenario is responsible for all game logic in an Action Field 
 * @author dalius
 *
 */

public interface Scenario {
	
	/**
	 * Disposes item from a game
	 * @param item
	 */
	
	public void disposeLineItem(LineItem item);
	
	/**
	 * Takes columns in row number and calculates index for filling
	 * line in the beginning of the game.
	 * @param columnsInTotal
	 * @return
	 */

	public LineItem createLineItem();
	
	public LineItem createMagazineLineItem();
	
	public int getInitRowIndex(int columnsInTotal);
	
	public ActionField getActionField();
	
	public Magazine getMagazine();
	
}
