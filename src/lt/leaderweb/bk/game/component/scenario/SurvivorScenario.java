package lt.leaderweb.bk.game.component.scenario;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

import lt.leaderweb.bk.game.Cell;
import lt.leaderweb.bk.game.Constants;
import lt.leaderweb.bk.game.GameManager;
import lt.leaderweb.bk.game.Level;
import lt.leaderweb.bk.game.VisualSettings;
import lt.leaderweb.bk.game.components.ActionField;
import lt.leaderweb.bk.game.components.Ball;
import lt.leaderweb.bk.game.components.GameRoot;
import lt.leaderweb.bk.game.components.GridGameLine;
import lt.leaderweb.bk.game.components.LineItem;
import lt.leaderweb.bk.game.components.Magazine;
import lt.leaderweb.bk.game.components.ScoreBoard;

public final class SurvivorScenario extends GridGameLine implements Scenario {

	private Stack<LineItem> celar = new Stack<LineItem>();
	private float defaultLineVectorY;
	private GameRoot root;
	
	public SurvivorScenario(Level level) {
		super(columnsOf(level));
		
		appendBlock();
		appendBlock();
		appendBlock();
	}
	
	static int columnsOf(Level level) {
		switch(level) {
		case insane: return 12;
		case hard: return 10;
		case medium: return 9;
		case easy:
		default:
			return 8;
		}
	}
	
	public void updateDisplay(float stopLine) {
		super.updateDisplay(stopLine);
		
		//1 ball line per 9 seconds
		defaultLineVectorY = -(VisualSettings.getCurrent().lineItemResolution*Constants.TickTimeoutInMs)/(9*1000);
		vectorY = defaultLineVectorY;
		
	}
	

	public LineItem createLineItem() {
		LineItem item;
		if (celar.empty()) {
			item = new Ball();
			randomise(item);
		} else { 
			item = celar.pop();
		}
		return item;
	}
	
	public LineItem createMagazineLineItem() {
		return createLineItem();
	}
	
	private void randomise(LineItem item) {
		item.setColor(LineItem.Color.values()[(int)Math.floor(Math.random()*4)]);
	}

	public void disposeLineItem(LineItem item) {
		if (item == null)
			return;
		randomise(item);
		celar.push(item);
	}

	public int getInitRowIndex(int columnsInTotal) {
		return (columnsInTotal/2)*5;
	}

	public LineItem getGunShell() {
		return this.createLineItem();
	}


	public final ActionField getActionField() {
		return this;
	}

	@Override
	protected void createLineItem(int x, int y) {
		super.addLineItem(createLineItem(), x, y);
	}
	
	protected synchronized void precessHit(final LineItem item, Cell shotCell, ScoreBoard scoreBoard) {

		this.addLineItem(item, shotCell.x, shotCell.y);
		Set<Cell> hitBag = new HashSet<Cell>();
		
		collectByColor(hitBag, item, shotCell);
		
		if (hitBag.size()<3)
			return;
		
		GameRoot root = getRoot();
		Set<Cell> fallBag = new HashSet<Cell>();
		LineItem handler = null;
		int i = 0;
		for (Cell cell: hitBag) {
			handler = grid[cell.y][cell.x];
			if (handler!=null) {
				scoreBoard.addScore(handler.getScore() + i++);
			}
			this.removeLineItem(cell, fallBag);
		}
		for (Cell cell: fallBag) {
			handler = grid[cell.y][cell.x];
			if (handler!=null) {
				handler.fall(getActualX(cell), getActualY(cell));
				root.registerAnimation(handler);
			}
			this.removeLineItem(cell.x, cell.y);
		}
		int y;
		lookup: for (y=0; y < rows-1; y++) {
			for (int x=1; x < columns+1; x++) {
				if (this.grid[y][x]!=null)
					break lookup;
			}
		}
		bottomLineIndex = y;
		
	}
	
	private GameRoot getRoot() {
		if (root==null) {
			root = GameManager.getInstance().getGameRunnable().getFrame();
		}
		return root;
	}
	
	/**
	 * Gather hitBag of effected LineItems
	 * @param hitBag
	 * @param item
	 * @param shotYIndex
	 * @param shotXIndex
	 * @param odd
	 */
	
	public void collectByColor(final Set<Cell> hitBag, final LineItem item, final Cell cell) {
		if (hitBag.contains(cell))
			return;
		
		if (item.isSameColor(this.grid[cell.y][cell.x])) {
			hitBag.add(cell);

			collectByColor(hitBag, item, cell.getLeft());
			collectByColor(hitBag, item, cell.getRight());
			
			if (getActualY(cell.getUpperA()) < stopLine) {
				collectByColor(hitBag, item, cell.getUpperA());
				collectByColor(hitBag, item, cell.getUpperB());
			}
			if (cell.y > 0) {
				collectByColor(hitBag, item, cell.getLowerA());
				collectByColor(hitBag, item, cell.getLowerB());
			}
		}
	}

	public Magazine getMagazine() {
		return new Magazine(this);
	}
}
