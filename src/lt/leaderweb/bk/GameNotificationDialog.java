package lt.leaderweb.bk;

import android.app.Dialog;
import android.view.View;
import android.widget.Button;

public class GameNotificationDialog extends Dialog {
	
	private final GameNotificationDialog dialog;

	public GameNotificationDialog(final GameActivity context, String message) {
		super(context);
		dialog = this;
		
		this.setCancelable(false);
		this.setCanceledOnTouchOutside(false);
		this.setTitle(message);
		
		setContentView(R.layout.notification);

		final Button dismissButton = (Button) findViewById(R.id.dismissButton);
		dismissButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				dialog.hide();
			}
		});
		final Button continueButton = (Button) findViewById(R.id.continueButton);
		continueButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				context.quitGame();
			}
		});
	}

}
