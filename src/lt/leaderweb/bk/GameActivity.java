package lt.leaderweb.bk;

import java.io.Serializable;

import lt.leaderweb.bk.game.GameFlowViewHandler;
import lt.leaderweb.bk.game.GameManager;
import lt.leaderweb.bk.game.Level;
import lt.leaderweb.bk.views.GameView;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class GameActivity extends AbstractActivity implements GameFlowViewHandler {

	private GameManager gameManager = GameManager.getInstance();
	private GameView view;
	private boolean optionsMenuOnResume = false;
	private final GameActivity activity;
	
	public GameActivity() {
		super();
		this.activity = this;
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
	        Serializable scenario = this.getIntent().getSerializableExtra("scenario");
	        Serializable level = this.getIntent().getSerializableExtra("level");
	        gameManager.loadGame((Class)scenario, (Level)level, this, getWindowManager().getDefaultDisplay());
	
	        view = new GameView(this, gameManager.getGameRunnable().getFrame());
	        setContentView(view);
        } catch (RuntimeException e) {
        	this.showErrorView(e);
        }
    }
    
    @Override
    protected void onDestroy() {
    	view.destroy();
    	super.onDestroy();
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
			case 0x00: 
				if (gameManager.isGameLoaded())
					gameManager.getGameRunnable().start();
				break;
			case 0x02:
				quitGame();
				break;
    		default:
    			return false;
    	}
    	return true;
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
    	menu.clear();
    	if (gameManager.isGameLoaded()) {
    		gameManager.getGameRunnable().pause();
    		switch (gameManager.getGameRunnable().getStatus()) {
				case WAITING:
			        menu.add(0, 0x00, 0, "Start Game");
					break;
				case PAUSED:
			        menu.add(0, 0x01, 0, "Resume Game");
			}
    	}

        menu.add(0, 0x02, 0, "Quit Game");
    	return super.onPrepareOptionsMenu(menu);
    }
    
    @Override
    public void onOptionsMenuClosed(Menu menu) {
    	if (gameManager.isGameLoaded()) {
    		gameManager.getGameRunnable().resume();
    	}
    }
    
    @Override
    public void onPause() {
    	if (gameManager.isGameLoaded()) {
    		gameManager.getGameRunnable().pause();
    	}
    	optionsMenuOnResume = true;
    	super.onPause();
    }
    
    @Override
    public void onResume() {
    	super.onResume();
    	if (optionsMenuOnResume) {
        	openOptionsMenu();
        	optionsMenuOnResume = false;
    	}
    }
    
    @Override
    public void onBackPressed() {
    	openOptionsMenu();
    }
    
    public void quitGame() {
		gameManager.disposeGame();
    	view.destroy();
    	Intent intent = new Intent(Intent.ACTION_VIEW);
    	intent.setClassName(this, MenuActivity.class.getName());
    	intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    	startActivity(intent);
    }

	public void showGameOverView() {
		runOnUiThread(new Runnable() {
			public void run() {
		        Dialog d = new GameNotificationDialog(activity, "Game Over");
		        d.show();
			}
		});
	}

	public void showErrorView(final RuntimeException e) {
		runOnUiThread(new Runnable() {
			public void run() {
		        Dialog d = new GameNotificationDialog(activity, "Unexpected error: " + e.getMessage() + " " + e.getCause());
		        d.show();
			}
		});
	}

	public void redraw() {
		if (view!=null)
			view.redraw();
	}

}
